#!/usr/bin/env bash

cd `dirname $0`

error() {
  echo "Failed !" >&2
  exit 1
}

echo "Compiling: loader.xml..."
swfmake -f loader.xml || error

echo "Compiling: swfmake.xml..."
swfmake -f swfmake.xml || error

echo "Done."
